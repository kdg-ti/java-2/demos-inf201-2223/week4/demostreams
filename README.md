Week 4 demo streams
-------------------
- De streams API is samen met lambda's toegevoegd in java en maakt er optimaal gebruik van
- Vanaf nu gaan we enkel nog via streams door collections gaan en niet langer via for lussen
- Een stream wordt aangemaakt met start operaties, dan gebruik je tussentijdse operaties zoals filter en map die een nieuwe stream geven en je eindigt met eindoperaties.
- voorbeelden van startoperaties: stream() en Stream.generate()
- voorbeelden van tussenoperaties: filter() en map() en limit()
- voorbeelden van eindoperaties: forEach(), sum() (enkel op getallenstreams), toList()
- findFirst() operatie is een eindoperatie die een Optional teruggeeft: te gebruiken als je functie mogelijk null teruggeeft (om nullpointers te vermijden!)
