package be.kdg.java2;

import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student(1, "jos", 1.98));
        students.add(new Student(2, "an", 1.8));
        students.add(new Student(3, "jef", 2));
        students.add(new Student(4, "marie", 1.67));
        students.add(new Student(5, "frank", 1.43));
        students.add(new Student(6, "wtine", 1.54));

        students.stream()
                .filter(s -> s.getLength() > 1.7)
                .forEach(System.out::println);

        int sum = students.stream()
                .filter(s -> s.getLength() > 1.7)
                .mapToInt(s -> s.getId())
                .sum();
        System.out.println("Som van de id's:" + sum);

        Optional<Student> studentOptional = Stream.generate(Student::generateRandom)
                .limit(10)
                .filter(s->s.getLength()>1.5)
                .sorted(Comparator.comparing(Student::getLength))
                .findFirst();
        System.out.println("Zijn lengte:" + studentOptional
                .orElse(new Student(-1,"empty",0)).getLength());

        List<Student> longStudents = students.stream()
                .filter(s -> s.getLength() > 1.7)
                .toList();
    }
}
